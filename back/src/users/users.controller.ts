import { Body, Controller, HttpCode, HttpException, HttpStatus, Post } from '@nestjs/common';
import { SignInDto, SignUpDto } from './dtos';
import { IResponse } from './../utils/response.util';
import { User } from '@prisma/client';
import { UsersService } from './users.service';
import { AuthenticationService } from './../authentication/authentication.service';

import * as argon from "argon2";

@Controller('users')
export class UsersController {
    constructor(
        private readonly usersService: UsersService,
        private readonly authenticationService: AuthenticationService
    ) {}

    @Post('/signin')
    @HttpCode(HttpStatus.OK)
    async signIn(@Body() body: SignInDto): Promise<IResponse> {
        const user: User | null = await this.usersService.getUserByEmail(body.email);

        const isCorrectPassword: boolean = user ? await argon.verify(user.password, body.password) : false;
        if (!isCorrectPassword) {
            throw new HttpException({ message: "The email address or the password is incorrect.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        return {
            status: HttpStatus.OK,
            message: 'User signed in.',
            data: await this.authenticationService.generateTokens(user)
        }
    }

    @Post('/signup')
    @HttpCode(HttpStatus.CREATED)
    async signUp(@Body() body: SignUpDto): Promise<IResponse> {
        if (body.administratorToken !== process.env.ADMINISTRATOR_TOKEN) {
            throw new HttpException({ message: "The administrator token is incorrect.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const user: User | null = await this.usersService.getUserByEmail(body.email);
        if (user) {
            throw new HttpException({ message: "The email address is already taken.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const hashedPassword: string = await argon.hash(body.password);
        const userCreated: User = await this.usersService.createUser(body.firstname, body.lastname, body.email, hashedPassword);

        return {
            status: HttpStatus.CREATED,
            message: 'User created.',
            data: userCreated
        }
    }
}