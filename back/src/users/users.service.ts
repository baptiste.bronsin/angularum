import { Injectable } from '@nestjs/common';
import { User } from '@prisma/client';
import prisma from 'src/utils/prisma.util';

@Injectable()
export class UsersService {
    async getUserById(id: string): Promise<User | null> {
        const user: User | null = await prisma.user.findUnique({
            where: {
                id: id
            }
        });

        return user;
    }

    async getUserByEmail(email: string): Promise<User | null> {
        const user: User | null = await prisma.user.findUnique({
            where: {
                email: email
            }
        });

        return user;
    }

    async createUser(firstname: string, lastname: string, email: string, password: string): Promise<User> {
        const user: User = await prisma.user.create({
            data: {
                firstname: firstname,
                lastname: lastname,
                email: email,
                password: password
            }
        });

        return user;
    }
}
