import { CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Request } from 'express';
import { JwtService } from '@nestjs/jwt';
import { ITokenPayload } from '../utils/token-payload.util';
import { IResponse } from '../utils/response.util';

@Injectable()
export class AuthenticationGuard implements CanActivate {
    constructor(
        private readonly jwtService: JwtService
    ) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const token = this.extractTokenFromHeader(request);

        if (!token) {
            throw new HttpException({ message: "This route is securised by a guard, provide an authentication token.", status: HttpStatus.UNAUTHORIZED } as IResponse, HttpStatus.UNAUTHORIZED);
        }

        let payload: ITokenPayload;

        try {
            payload = await this.jwtService.verifyAsync(
                token,
                { secret: process.env.JWT_ACCESS_SECRET }
            );
        } catch {
            throw new HttpException({ message: "Your token is not correct.", status: HttpStatus.UNAUTHORIZED }  as IResponse, HttpStatus.UNAUTHORIZED);
        }
        
        request['jwtUser'] = payload;
        
        return true;
    }

    private extractTokenFromHeader(request: Request): string | undefined {
        const [type, token] = request.headers.authorization?.split(' ') ?? [];
        return type === 'Bearer' ? token : undefined;
    }

}
