export interface ITokenPayload {
    sub: string;
    user: {
        id: string;
        firstname: string;
        lastname: string;
        email: string;
    }
}