import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import prisma from './utils/prisma.util';
import { exec } from 'child_process';

async function prismaMigration(): Promise<void> {
  return new Promise((resolve, reject) => {
      exec('npm run migrate:prod', (error, stdout) => {
          if (error) {
              console.log(error);
              reject(error);
          }
          console.log(stdout);
          resolve();
      });
  });
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(new ValidationPipe({
    whitelist: true,
    forbidNonWhitelisted: true
  }));

  console.log('Connexion à la base de données...');
  await prisma.$connect();
  await prismaMigration();

  app.enableCors();

  await app.listen(process.env.PORT || 3000);
}
bootstrap();
