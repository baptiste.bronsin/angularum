import { Injectable } from '@nestjs/common';
import { JwtService } from "@nestjs/jwt";
import { ITokenPayload } from '../utils/token-payload.util';
import { User } from '@prisma/client';

@Injectable()
export class AuthenticationService {
    constructor(
      private readonly jwtService: JwtService
    ) {}

    async generateTokens(user: User): Promise<{ accessToken: string, refreshToken: string }> {
        const payload: ITokenPayload = {
          sub: user.id,
          user: {
            id: user.id,
            firstname: user.firstname,
            lastname: user.lastname,
            email: user.email,
          }
        };
    
        const accessToken = await this.jwtService.signAsync(payload, {
          secret: process.env.JWT_ACCESS_SECRET,
          expiresIn: process.env.JWT_ACCESS_EXPIRATION,
        });
    
        const refreshToken = await this.jwtService.signAsync(payload, {
          secret: process.env.JWT_REFRESH_SECRET,
          expiresIn: process.env.JWT_REFRESH_EXPIRATION,
        });
    
        return {
            accessToken: accessToken,
            refreshToken: refreshToken,
        };
    }

    async verifyRefreshToken(refreshToken: string): Promise<ITokenPayload | null> {
      try {
        const payload = await this.jwtService.verifyAsync(
          refreshToken,
          { secret: process.env.JWT_REFRESH_SECRET }
        );
  
        return payload;
      } catch {
        return null;
      }
    }
}
