import { IsDateString, IsNotEmpty, IsOptional, IsString } from "class-validator";

export class UpdateTopicDto {
    @IsString()
    @IsNotEmpty()
    id: string;

    @IsString()
    @IsNotEmpty()
    title: string;

    @IsString()
    @IsNotEmpty()
    description: string;
}