import { Module } from '@nestjs/common';
import { TopicsController } from './topics.controller';
import { TopicsService } from './topics.service';
import { UsersService } from 'src/users/users.service';
import { AuthenticationService } from 'src/authentication/authentication.service';
import { JwtService } from '@nestjs/jwt';

@Module({
  controllers: [TopicsController],
  providers: [TopicsService, UsersService, AuthenticationService, JwtService]
})
export class TopicsModule {}
