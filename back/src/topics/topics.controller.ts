import { Body, Controller, Get, HttpException, HttpStatus, Post, Query, UseGuards, Request, Patch, Delete, Param } from '@nestjs/common';
import { IResponse } from 'src/utils/response.util';
import { TopicsService } from './topics.service';
import { CreateTopicDto, UpdateTopicDto } from './dtos';
import { Topic, User } from '@prisma/client';
import { AuthenticationGuard } from 'src/guards/authentication.guard';
import { UsersService } from 'src/users/users.service';

@Controller('topics')
@UseGuards(AuthenticationGuard)
export class TopicsController {
    constructor(
        private readonly topicsService: TopicsService,
        private readonly usersService: UsersService
    ) {}

    @Get('/')
    async findByParameters(@Request() req, @Query('page') pageParam: string, @Query('author') authorIdParam: string, @Query('title') titleParam: string, @Query('isOpen') isOpenParam: string): Promise<IResponse> {
        const user: User | null = await this.usersService.getUserById(req.jwtUser.sub);
        if (!user) {
            throw new HttpException({ message: "Any user has been found with this token.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (!pageParam || isNaN(parseInt(pageParam))) {
            throw new HttpException({ message: "Provide a page number.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (authorIdParam && authorIdParam != '') {
            const author: User | null = await this.usersService.getUserById(authorIdParam);
            if (!author) {
                throw new HttpException({ message: "Any user has been found with this author id.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
            }
        }

        if (isOpenParam !== undefined) {
            if (isOpenParam !== 'true' && isOpenParam !== 'false') {
                throw new HttpException({ message: "isOpen must be a boolean.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
            }
        }

        const page: number = Number(pageParam);
        const limit: number = 10;

        return {
            status: HttpStatus.OK,
            message: 'Topics fetched.',
            data: {
                pagination : {
                    page: page,
                    limit: limit,
                    totalAmount: (await this.topicsService.findByParameters(authorIdParam, titleParam, isOpenParam !== undefined ? isOpenParam == 'true' : undefined)).length
                },
                topics: await this.topicsService.findByParameters(authorIdParam, titleParam, isOpenParam !== undefined ? isOpenParam == 'true' : undefined, page)
            }
        }
    }

    @Get('/:id')
    async findOneById(@Request() req, @Param('id') idParam: string): Promise<IResponse> {
        const user: User | null = await this.usersService.getUserById(req.jwtUser.sub);
        if (!user) {
            throw new HttpException({ message: "Any user has been found with this token.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (!idParam) {
            throw new HttpException({ message: "Provide an id.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const topic: Topic | null = await this.topicsService.findOneById(idParam);
        if (!topic) {
            throw new HttpException({ message: "Any topic has been found with this id.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        return {
            status: HttpStatus.OK,
            message: 'Topic fetched.',
            data: topic
        }
    }

    @Post('/')
    async create(@Request() req, @Body() body: CreateTopicDto): Promise<IResponse> {
        const user: User | null = await this.usersService.getUserById(req.jwtUser.sub);
        if (!user) {
            throw new HttpException({ message: "Any user has been found with this token.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const topic: Topic = await this.topicsService.create(body.title, body.description, user.id);

        return {
            status: HttpStatus.CREATED,
            message: 'Topic created.',
            data: topic
        }
    }

    @Patch('/')
    async update(@Request() req, @Body() body: UpdateTopicDto): Promise<IResponse> {
        const user: User | null = await this.usersService.getUserById(req.jwtUser.sub);
        if (!user) {
            throw new HttpException({ message: "Any user has been found with this token.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const topic: Topic | null = await this.topicsService.findOneById(body.id);
        if (!topic) {
            throw new HttpException({ message: "Any topic has been found with this id.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (topic.authorId !== user.id) {
            throw new HttpException({ message: "You are not the author of this topic.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const updatedTopic: Topic = await this.topicsService.update(body.id, body.title, body.description, topic.closedAt);

        return {
            status: HttpStatus.OK,
            message: 'Topic updated.',
            data: updatedTopic
        }
    }

    @Patch('/:id/close')
    async closeTopic(@Request() req, @Param('id') idParam: string): Promise<IResponse> {
        const user: User | null = await this.usersService.getUserById(req.jwtUser.sub);
        if (!user) {
            throw new HttpException({ message: "Any user has been found with this token.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (!idParam) {
            throw new HttpException({ message: "Provide an id.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const topic: Topic = await this.topicsService.findOneById(idParam);
        if (!topic) {
            throw new HttpException({ message: "Any topic has been found with this id.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (topic.authorId !== user.id) {
            throw new HttpException({ message: "You are not the author of this topic.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (topic.closedAt) {
            throw new HttpException({ message: "This topic is already closed.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const updatedTopic: Topic = await this.topicsService.update(topic.id, topic.title, topic.description, new Date());

        return {
            status: HttpStatus.OK,
            message: 'Topic closed.',
            data: updatedTopic
        }
    }

    @Patch('/:id/reopen')
    async reopenTopic(@Request() req, @Param('id') idParam: string): Promise<IResponse> {
        const user: User | null = await this.usersService.getUserById(req.jwtUser.sub);
        if (!user) {
            throw new HttpException({ message: "Any user has been found with this token.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (!idParam) {
            throw new HttpException({ message: "Provide an id.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const topic: Topic = await this.topicsService.findOneById(idParam);
        if (!topic) {
            throw new HttpException({ message: "Any topic has been found with this id.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (topic.authorId !== user.id) {
            throw new HttpException({ message: "You are not the author of this topic.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (!topic.closedAt) {
            throw new HttpException({ message: "This topic is already opened.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const updatedTopic: Topic = await this.topicsService.update(topic.id, topic.title, topic.description, null);

        return {
            status: HttpStatus.OK,
            message: 'Topic reopened.',
            data: updatedTopic
        }
    }

    @Delete('/:id')
    async delete(@Request() req, @Param('id') idParam: string): Promise<IResponse> {
        const user: User | null = await this.usersService.getUserById(req.jwtUser.sub);
        if (!user) {
            throw new HttpException({ message: "Any user has been found with this token.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (!idParam) {
            throw new HttpException({ message: "Provide an id.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }
        
        const topic: Topic | null = await this.topicsService.findOneById(idParam);
        if (!topic) {
            throw new HttpException({ message: "Any topic has been found with this id.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (topic.authorId !== user.id) {
            throw new HttpException({ message: "You are not the author of this topic.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        await this.topicsService.delete(idParam);

        return {
            status: HttpStatus.OK,
            message: 'Topic deleted.',
            data: null
        }
    }
}
