import { Injectable } from '@nestjs/common';
import { Topic } from '@prisma/client';
import prisma from 'src/utils/prisma.util';

@Injectable()
export class TopicsService {
    constructor() {}

    async findByParameters(authorId: string | undefined, title: string | undefined, isOpen: boolean | undefined, page?: number): Promise<Topic[]> {
        if (page === undefined)
            return await prisma.topic.findMany({
                where: {
                    authorId: authorId ?? undefined,
                    title: title ? {
                        contains: title,
                        mode: 'insensitive'
                    } : undefined,
                    closedAt: isOpen === undefined ? undefined : isOpen ? null : { not: null }
                },
                orderBy: {
                    createdAt: 'desc'
                },
                include: {
                    author: {
                        select: {
                            id: true,
                            firstname: true,
                            lastname: true,
                            email: true
                        }
                    }
                }
            });
        else
            return await prisma.topic.findMany({
                where: {
                    authorId: authorId ?? undefined,
                    title: title ? {
                        contains: title,
                        mode: 'insensitive'
                    } : undefined,
                    closedAt: isOpen === undefined ? undefined : isOpen ? null : { not: null }
                },
                skip: (page - 1) * 10,
                take: 10,
                orderBy: {
                    createdAt: 'desc'
                },
                include: {
                    author: {
                        select: {
                            id: true,
                            firstname: true,
                            lastname: true,
                            email: true
                        }
                    }
                }
            });
    }

    async findOneById(id: string): Promise<Topic | null> {
        return await prisma.topic.findUnique({
            where: {
                id: id
            },
            include: {
                author: {
                    select: {
                        id: true,
                        firstname: true,
                        lastname: true,
                        email: true
                    }
                }
            }
        });
    }

    async create(title: string, description: string, authorId: string): Promise<Topic> {
        return await prisma.topic.create({
            data: {
                title: title,
                description: description,
                authorId: authorId
            },
            include: {
                author: {
                    select: {
                        id: true,
                        firstname: true,
                        lastname: true,
                        email: true
                    }
                }
            }
        });
    }

    async update(id: string, title: string, description: string, closedAt: Date | null): Promise<Topic> {
        return await prisma.topic.update({
            where: {
                id: id
            },
            data: {
                title: title,
                description: description,
                closedAt: closedAt
            },
            include: {
                author: {
                    select: {
                        id: true,
                        firstname: true,
                        lastname: true,
                        email: true
                    }
                }
            }
        });
    }

    async delete(id: string): Promise<Topic> {
        await prisma.post.deleteMany({
            where: {
                topicId: id
            }
        });

        return await prisma.topic.delete({
            where: {
                id: id
            }
        });
    }
}
