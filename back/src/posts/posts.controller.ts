import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Patch, Post, Query, Request, UseGuards } from '@nestjs/common';
import { AuthenticationGuard } from 'src/guards/authentication.guard';
import { PostsService } from './posts.service';
import { TopicsService } from 'src/topics/topics.service';
import { UsersService } from 'src/users/users.service';
import { Post as PrismaPost, Topic, User } from '@prisma/client';
import { IResponse } from 'src/utils/response.util';
import { CreatePostDto, UpdatePostDto } from './dtos';

@Controller('posts')
@UseGuards(AuthenticationGuard)
export class PostsController {
    constructor(
        private readonly postsService: PostsService,
        private readonly topicsService: TopicsService,
        private readonly usersService: UsersService
    ) {}

    @Get('/topic/:topicId')
    async findAllByTopicId(@Request() req, @Param('topicId') topicId: string, @Query('content') contentParam: string, @Query('page') pageParam?: string): Promise<IResponse> {
        const user: User | null = await this.usersService.getUserById(req.jwtUser.sub);
        if (!user) {
            throw new HttpException({ message: "Any user has been found with this token.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (!topicId) {
            throw new HttpException({ message: "Provide an id.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (!pageParam || isNaN(parseInt(pageParam))) {
            throw new HttpException({ message: "Provide a page number.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const topic: Topic | null = await this.topicsService.findOneById(topicId);
        if (!topic) {
            throw new HttpException({ message: "Any topic has been found with this id.", status: HttpStatus.NOT_FOUND } as IResponse, HttpStatus.NOT_FOUND);
        }

        const page: number = Number(pageParam);
        const limit: number = 10;

        return {
            status: HttpStatus.OK,
            message: "Posts fetched.",
            data: {
                pagination: {
                    page: page,
                    limit: limit,
                    totalAmount: (await this.postsService.findAllByParams(topicId, contentParam, req.jwtUser.sub === user.id)).length
                },
                posts: await this.postsService.findAllByParams(topicId, contentParam, req.jwtUser.sub === user.id, page)
            }
        }
    }

    @Get('/author/:authorId')
    async findByAuthorId(@Request() req, @Param('authorId') authorId: string, @Query('page') pageParam: string): Promise<IResponse> {
        const user: User | null = await this.usersService.getUserById(req.jwtUser.sub);
        if (!user) {
            throw new HttpException({ message: "Any user has been found with this token.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (!authorId) {
            throw new HttpException({ message: "Provide an author id.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (!pageParam || isNaN(parseInt(pageParam))) {
            throw new HttpException({ message: "Provide a page number.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const author: User | null = await this.usersService.getUserById(authorId);
        if (!author) {
            throw new HttpException({ message: "Any user has been found with this author id.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const page: number = Number(pageParam);
        const limit: number = 10;

        return {
            status: HttpStatus.OK,
            message: "Posts fetched.",
            data: {
                pagination: {
                    page: page,
                    limit: limit,
                    totalAmount: (await this.postsService.findByAuthorId(authorId, req.jwtUser.sub === user.id)).length
                },
                posts: await this.postsService.findByAuthorId(authorId, req.jwtUser.sub === user.id, page)
            }
        }
    }

    @Get('/:id')
    async findOneById(@Request() req, @Param('id') idParam: string): Promise<IResponse> {
        const user: User | null = await this.usersService.getUserById(req.jwtUser.sub);
        if (!user) {
            throw new HttpException({ message: "Any user has been found with this token.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (!idParam) {
            throw new HttpException({ message: "Provide an id.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const post: PrismaPost | null = await this.postsService.findOneById(idParam);
        if (!post) {
            throw new HttpException({ message: "Any post has been found with this id.", status: HttpStatus.NOT_FOUND } as IResponse, HttpStatus.NOT_FOUND);
        }

        return {
            status: HttpStatus.OK,
            message: "Post fetched.",
            data: post
        }
    }

    @Post('/')
    async create(@Request() req, @Body() body: CreatePostDto): Promise<IResponse> {
        const user: User | null = await this.usersService.getUserById(req.jwtUser.sub);
        if (!user) {
            throw new HttpException({ message: "Any user has been found with this token.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const topic: Topic | null = await this.topicsService.findOneById(body.topicId);
        if (!topic) {
            throw new HttpException({ message: "Any topic has been found with this id.", status: HttpStatus.NOT_FOUND } as IResponse, HttpStatus.NOT_FOUND);
        }

        const post: PrismaPost = await this.postsService.create(user.id, body.topicId, body.content);

        return {
            status: HttpStatus.CREATED,
            message: "Post created.",
            data: post
        }
    }

    @Patch('/')
    async update(@Request() req, @Body() body: UpdatePostDto): Promise<IResponse> {
        const user: User | null = await this.usersService.getUserById(req.jwtUser.sub);
        if (!user) {
            throw new HttpException({ message: "Any user has been found with this token.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const post: PrismaPost | null = await this.postsService.findOneById(body.id);
        if (!post) {
            throw new HttpException({ message: "Any post has been found with this id.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (post.authorId !== user.id) {
            throw new HttpException({ message: "You are not the author of this post.", status: HttpStatus.FORBIDDEN } as IResponse, HttpStatus.FORBIDDEN);
        }

        const updatedPost: PrismaPost = await this.postsService.update(body.id, body.content, post.hidden);

        return {
            status: HttpStatus.OK,
            message: "Post updated.",
            data: updatedPost
        }
    }

    @Patch('/hide/:id')
    async hide(@Request() req, @Param('id') idParam: string): Promise<IResponse> {
        const user: User | null = await this.usersService.getUserById(req.jwtUser.sub);
        if (!user) {
            throw new HttpException({ message: "Any user has been found with this token.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (!idParam) {
            throw new HttpException({ message: "Provide an id.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const post: PrismaPost | null = await this.postsService.findOneById(idParam);
        if (!post) {
            throw new HttpException({ message: "Any post has been found with this id.", status: HttpStatus.NOT_FOUND } as IResponse, HttpStatus.NOT_FOUND);
        }

        if (post.authorId !== user.id) {
            throw new HttpException({ message: "You are not the author of this post.", status: HttpStatus.FORBIDDEN } as IResponse, HttpStatus.FORBIDDEN);
        }

        if (post.hidden) {
            throw new HttpException({ message: "Post is already hidden.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const updatedPost: PrismaPost = await this.postsService.update(idParam, post.content, true);

        return {
            status: HttpStatus.OK,
            message: "Post hidden.",
            data: updatedPost
        }
    }

    @Patch('/unhide/:id')
    async unhide(@Request() req, @Param('id') idParam: string): Promise<IResponse> {
        const user: User | null = await this.usersService.getUserById(req.jwtUser.sub);
        if (!user) {
            throw new HttpException({ message: "Any user has been found with this token.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (!idParam) {
            throw new HttpException({ message: "Provide an id.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const post: PrismaPost | null = await this.postsService.findOneById(idParam);
        if (!post) {
            throw new HttpException({ message: "Any post has been found with this id.", status: HttpStatus.NOT_FOUND } as IResponse, HttpStatus.NOT_FOUND);
        }

        if (post.authorId !== user.id) {
            throw new HttpException({ message: "You are not the author of this post.", status: HttpStatus.FORBIDDEN } as IResponse, HttpStatus.FORBIDDEN);
        }

        if (!post.hidden) {
            throw new HttpException({ message: "Post is already unhidden.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const updatedPost: PrismaPost = await this.postsService.update(idParam, post.content, false);

        return {
            status: HttpStatus.OK,
            message: "Post unhidden.",
            data: updatedPost
        }
    }

    @Delete('/:id')
    async delete(@Request() req, @Param('id') idParam: string): Promise<IResponse> {
        const user: User | null = await this.usersService.getUserById(req.jwtUser.sub);
        if (!user) {
            throw new HttpException({ message: "Any user has been found with this token.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        if (!idParam) {
            throw new HttpException({ message: "Provide an id.", status: HttpStatus.BAD_REQUEST } as IResponse, HttpStatus.BAD_REQUEST);
        }

        const post: PrismaPost | null = await this.postsService.findOneById(idParam);
        if (!post) {
            throw new HttpException({ message: "Any post has been found with this id.", status: HttpStatus.NOT_FOUND } as IResponse, HttpStatus.NOT_FOUND);
        }

        if (post.authorId !== user.id) {
            throw new HttpException({ message: "You are not the author of this post.", status: HttpStatus.FORBIDDEN } as IResponse, HttpStatus.FORBIDDEN);
        }

        await this.postsService.delete(idParam);

        return {
            status: HttpStatus.OK,
            message: "Post deleted.",
            data: null
        }
    }
}
