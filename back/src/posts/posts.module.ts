import { Module } from '@nestjs/common';
import { PostsController } from './posts.controller';
import { PostsService } from './posts.service';
import { UsersService } from 'src/users/users.service';
import { JwtService } from '@nestjs/jwt';
import { TopicsService } from 'src/topics/topics.service';

@Module({
  controllers: [PostsController],
  providers: [PostsService, UsersService, JwtService, TopicsService]
})
export class PostsModule {}
