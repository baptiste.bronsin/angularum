import { Injectable } from '@nestjs/common';
import { Post } from '@prisma/client';
import prisma from 'src/utils/prisma.util';

@Injectable()
export class PostsService {
    constructor() {}

    async findAllByParams(topicId: string, content: string | undefined, includeHidden: boolean, page?: number): Promise<Post[]> {
        if (page === undefined) 
            return prisma.post.findMany({
                where: {
                    topicId: topicId,
                    content: content ? {
                        contains: content,
                        mode: 'insensitive'
                    } : undefined,
                    hidden: includeHidden ? undefined : false
                },
                orderBy: {
                    createdAt: 'desc'
                },
                include: {
                    author: {
                        select: {
                            id: true,
                            firstname: true,
                            lastname: true,
                            email: true
                        }
                    },
                    topic: {
                        select: {
                            id: true,
                            title: true
                        }
                    }
                }
            })
        else
            return prisma.post.findMany({
                where: {
                    topicId: topicId,
                    content: content ? {
                        contains: content,
                        mode: 'insensitive'
                    } : undefined,
                    hidden: includeHidden ? undefined : false
                },
                skip: (page - 1) * 10,
                take: 10,
                orderBy: {
                    createdAt: 'desc'
                },
                include: {
                    author: {
                        select: {
                            id: true,
                            firstname: true,
                            lastname: true,
                            email: true
                        }
                    },
                    topic: {
                        select: {
                            id: true,
                            title: true
                        }
                    }
                }
            })
    }

    async findOneById(id: string): Promise<Post | null> {
        return prisma.post.findUnique({
            where: {
                id: id
            },
            include: {
                author: {
                    select: {
                        id: true,
                        firstname: true,
                        lastname: true,
                        email: true
                    }
                },
                topic: {
                    select: {
                        id: true,
                        title: true
                    }
                }
            }
        })
    }

    async findByAuthorId(authorId: string, includeHidden: boolean, page?: number): Promise<Post[]> {
        if (page === undefined) 
            return prisma.post.findMany({
                where: {
                    authorId: authorId,
                    hidden: includeHidden ? undefined : false
                },
                orderBy: {
                    createdAt: 'desc'
                },
                include: {
                    author: {
                        select: {
                            id: true,
                            firstname: true,
                            lastname: true,
                            email: true
                        }
                    },
                    topic: {
                        select: {
                            id: true,
                            title: true
                        }
                    }
                }
            })
        else
            return prisma.post.findMany({
                where: {
                    authorId: authorId,
                    hidden: includeHidden ? undefined : false
                },
                skip: (page - 1) * 10,
                take: 10,
                orderBy: {
                    createdAt: 'desc'
                },
                include: {
                    author: {
                        select: {
                            id: true,
                            firstname: true,
                            lastname: true,
                            email: true
                        }
                    },
                    topic: {
                        select: {
                            id: true,
                            title: true
                        }
                    }
                }
            })
    }

    async findByAuthorIdAndTopicId(authorId: string, topicId: string, includeHidden: boolean, page?: number): Promise<Post[]> {
        if (page === undefined) 
            return prisma.post.findMany({
                where: {
                    topicId: topicId,
                    authorId: authorId,
                    hidden: includeHidden ? undefined : false
                },
                orderBy: {
                    createdAt: 'desc'
                },
                include: {
                    author: {
                        select: {
                            id: true,
                            firstname: true,
                            lastname: true,
                            email: true
                        }
                    },
                    topic: {
                        select: {
                            id: true,
                            title: true
                        }
                    }
                }
            })
        else
            return prisma.post.findMany({
                where: {
                    topicId: topicId,
                    authorId: authorId,
                    hidden: includeHidden ? undefined : false
                },
                skip: (page - 1) * 10,
                take: 10,
                orderBy: {
                    createdAt: 'desc'
                },
                include: {
                    author: {
                        select: {
                            id: true,
                            firstname: true,
                            lastname: true,
                            email: true
                        }
                    },
                    topic: {
                        select: {
                            id: true,
                            title: true
                        }
                    }
                }
            })
    }

    async create(authorId: string, topicId: string, content: string): Promise<Post> {
        return prisma.post.create({
            data: {
                authorId: authorId,
                topicId: topicId,
                content: content
            },
            include: {
                author: {
                    select: {
                        id: true,
                        firstname: true,
                        lastname: true,
                        email: true
                    }
                },
                topic: {
                    select: {
                        id: true,
                        title: true
                    }
                }
            }
        })
    }

    async update(id: string, content: string, hidden: boolean): Promise<Post> {
        return prisma.post.update({
            where: {
                id: id
            },
            data: {
                content: content,
                hidden: hidden
            },
            include: {
                author: {
                    select: {
                        id: true,
                        firstname: true,
                        lastname: true,
                        email: true
                    }
                },
                topic: {
                    select: {
                        id: true,
                        title: true
                    }
                }
            }
        })
    }

    async delete(id: string): Promise<Post> {
        return prisma.post.delete({
            where: {
                id: id
            }
        })
    }
}
