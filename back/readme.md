## Angularum - backend

Ce fichier répertorie tous les endpoints disponibles.

### APIS

**Utilisateur**
- Signup : `POST /users/signup`
```json
{
    "firstname": "string",
    "lastname": "string",
    "email": "string",
    "password": "string",
    "administratorToken": "string from the back env file"
}
```

- Signin : `POST /users/signin`
```json
{
    "email": "string",
    "password": "string"
}
```

**Topic**
- Tous les topics : `GET /topics?page=:pageParam&author=:authorIdParam&title=:titleParam&isOpen=:isOpenParam`
  `pageParam` est un paramètre (nombre) dans l'url.
  `authorIdParam` est un paramètre facultatif (string) dans l'url.
  `titleParam` est un paramètre facultatif (string) dans l'url.
  `isOpenParam` est un paramètre facultatif (boolean) dans l'url.


- Un topic par ID : `GET /topics/:topicId`

- Créer un topic : `POST /topics`
```json
{
    "title": "string",
    "description": "string"
}
```

- Mettre à jour un topic : `PATCH /topics`
```json
{
    "id": "string",
    "title": "string",
    "description": "string"
}
```

- Supprimer un topic par ID : `DELETE /topics/:topicId`

- Fermer un topic par ID : `PATCH /topics/:topicId/close`

- Réouvrir un topic par ID : `PATCH /topics/:topicId/reopen`

**Post**
- Tous les posts d'un topic : `GET /posts/topic/:topicId?page=:pageParam&content=:contentParam`
  `pageParam` est un paramètre (nombre) dans l'url.
  `contentParam` est un paramètre facultatif (string) dans l'url.

- Un post par ID : `GET /posts/:postId`

- Tous les posts par auteur ID : `GET /posts/author/:authorId?page=:pageParam`
  `pageParam` est un paramètre (nombre) dans l'url.

- Créer un post : `POST /posts`
```json
{
    "content": "string",
    "topicId": "string"
}
```

- Mettre à jour un post : `PATCH /posts`
```json
{
    "id": "string",
    "content": "string"
}
```

- Cacher un post par ID : `PATCH /posts/hide/:postId`

- Afficher un post par ID : `PATCH /posts/unhide/:postId`

- Supprimer un post par ID : `DELETE /posts/:postId`