import { Component, OnDestroy, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { LucideAngularModule } from 'lucide-angular';
import { AsyncPipe, CommonModule } from '@angular/common';
import { AuthenticateService } from './features/authentication/services/authenticate.service';
import { Tokens } from './shared/models/tokens.model';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, LucideAngularModule, CommonModule, ToastModule, AsyncPipe],
  providers: [MessageService],
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  private payloadSubject$: BehaviorSubject<{ firstname: string; lastname: string } | undefined>;
  payload$: Observable<{ firstname: string; lastname: string } | undefined>;

  constructor(
    public authenticateService: AuthenticateService
  ) {
    this.payloadSubject$ = new BehaviorSubject<{ firstname: string; lastname: string } | undefined>(undefined);
    this.payload$ = this.payloadSubject$.asObservable();
  }

  ngOnInit(): void {
    this.authenticateService.tokens.subscribe((tokens: Tokens | null) => {
      if (tokens) {
        const tokenPayload = this.authenticateService.getDecodedAccessToken();
        console.log('tokenPayload',tokenPayload);
        if (tokenPayload !== null && tokenPayload.user !== undefined && tokenPayload.user.firstname !== undefined && tokenPayload.user.lastname !== undefined) {
          this.payloadSubject$.next({ firstname: tokenPayload.user.firstname, lastname: tokenPayload.user.lastname });
        }
      } else {
        this.payloadSubject$.next(undefined);
      }
    });
  }

  onLogout(): void {
    this.authenticateService.logout();
  }
}
