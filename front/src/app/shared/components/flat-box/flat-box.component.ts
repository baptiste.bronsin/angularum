import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-flat-box',
  standalone: true,
  templateUrl: './flat-box.component.html'
})
export class FlatBoxComponent {
    
  @Input() title: string | undefined = undefined;

}
