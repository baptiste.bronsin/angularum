import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-main-button',
  standalone: true,
  imports: [],
  templateUrl: './main-button.component.html'
})
export class MainButtonComponent {
    
  @Input() label: string = 'Click me';
  @Input() isDisabled: boolean = false;
  @Input() isLoading: boolean = false;
  @Output() onClick = new EventEmitter<void>();

  handleClick() {
    if (!this.isDisabled && !this.isLoading) {
      this.onClick.emit();
    }
  }
}
