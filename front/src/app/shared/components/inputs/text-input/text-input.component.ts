import { Component, Input, forwardRef } from '@angular/core';
import { ReactiveFormsModule, NG_VALUE_ACCESSOR, ControlValueAccessor, FormGroup } from '@angular/forms';
import { LucideAngularModule } from 'lucide-angular';

@Component({
  selector: 'app-text-input',
  standalone: true,
  imports: [LucideAngularModule, ReactiveFormsModule],
  templateUrl: './text-input.component.html',
  providers: [
    {
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => TextInputComponent),
        multi: true
    }
  ]
})
export class TextInputComponent implements ControlValueAccessor {

  @Input() type: 'text' | 'email' | 'password' = 'text';
  @Input() placeholder: string = "";
  @Input() formControlName: string = "";
  @Input() maxLength: number | undefined = undefined;
  @Input() isError: boolean = false;
  @Input() guestForm: FormGroup = new FormGroup({});

  showPassword: boolean = false;
  value: string = '';

  onChange = (value: string) => {};
  onTouched = () => {};

  toggleShowPassword() {
    this.showPassword = !this.showPassword;
  }

  writeValue(value: string): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    // Handle the disabled state if needed
  }
}
