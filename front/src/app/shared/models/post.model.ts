import { TopicLight } from "./topic.model"
import { User } from "./user.model"

export class Post {
    id: string;
    content: string;
    authorId: string;
    topicId: string;
    createdAt: Date;
    updatedAt: Date;
    hidden: boolean;
    author: User;
    topic: TopicLight;

    constructor(
        id: string,
        content: string,
        authorId: string,
        topicId: string,
        createdAt: Date,
        updatedAt: Date,
        hidden: boolean,
        author: User,
        topic: TopicLight
    ) {
        this.id = id;
        this.content = content;
        this.authorId = authorId;
        this.topicId = topicId;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.hidden = hidden;
        this.author = author;
        this.topic = topic;
    }
}