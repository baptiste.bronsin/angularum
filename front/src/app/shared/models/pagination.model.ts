export class Pagination {
    constructor(
        public page: number,
        public limit: number,
        public totalAmount: number
    ) {}
}