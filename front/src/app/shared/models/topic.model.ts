import { User } from "./user.model";

export class Topic {
    id: string;
    title: string;
    description: string;
    authorId: string;
    createdAt: Date;
    updatedAt: Date;
    closedAt: Date | null;
    author: User;

    constructor(
        id: string,
        title: string,
        description: string,
        authorId: string,
        createdAt: Date,
        updatedAt: Date,
        closedAt: Date | null,
        author: User
    ) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.authorId = authorId;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.closedAt = closedAt;
        this.author = author;
    }
}

export class TopicLight {
    constructor(
        id: string,
        title: string
    ) {}
}