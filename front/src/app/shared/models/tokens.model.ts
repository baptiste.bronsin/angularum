export class Tokens {
    constructor(
        public accessToken: string,
        public refreshToken: string
    ) {}
}