import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthenticateService } from "../../features/authentication/services/authenticate.service";
import { Observable } from "rxjs";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(
        private authenticateService: AuthenticateService
    ) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const tokens = this.authenticateService.getTokens();

        if (tokens) {
            req = req.clone({
                setHeaders: {
                    Authorization: `Bearer ${tokens.accessToken}`
                }
            });
        } else {
            req = req.clone({
                setHeaders: {
                    Authorization: ''
                }
            });
        }

        return next.handle(req);
    }
}