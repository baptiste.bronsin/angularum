import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router, RouterOutlet } from '@angular/router';
import { LucideAngularModule } from 'lucide-angular';
import { FlatBoxComponent } from '../../../shared/components/flat-box/flat-box.component';
import { TextInputComponent } from '../../../shared/components/inputs/text-input/text-input.component';
import { MainButtonComponent } from '../../../shared/components/buttons/main-button/main-button.component';
import { lastValueFrom } from 'rxjs';
import { PostService } from '../services/post.service';
import { Topic } from '../../../shared/models/topic.model';
import { TopicService } from '../../topics/services/topic.service';

@Component({
  selector: 'app-add-post',
  standalone: true,
  imports: [RouterOutlet, ReactiveFormsModule, FlatBoxComponent, TextInputComponent, MainButtonComponent, LucideAngularModule, FlatBoxComponent],
  providers: [],
  templateUrl: '../pages/add-post.component.html'
})
export class AddPostComponent implements OnInit {
  constructor(
    private postService: PostService,
    private topicService: TopicService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  topic: Topic | undefined | null = undefined;

  postCreationForm = new FormGroup({
    content: new FormControl('')
  })
  error: string | undefined = undefined;

  async getTopic(topicId: string) {
    try {
      const topicResult = await this.topicService.getTopicById(topicId);

      if (topicResult === undefined) {
        this.topic = null;
        console.error('No topic found');
        return;
      }

      this.topic = await lastValueFrom(topicResult);
    } catch (error) {
      this.topic = null;
    }
  }

  async onSubmit() {
    if (!this.topic) {return}
    if (this.postCreationForm.invalid || !this.postCreationForm.value.content) {
        this.error = 'Veuillez saisir tous les champs de saisies';
        return
      }
  
      try {
        const resultObs = await this.postService.addPost(this.topic.id, this.postCreationForm.value.content);
        if (!resultObs) {
            this.error = 'Une erreur est survenue lors de la création du post';
            return;
        }

        const result = await lastValueFrom(resultObs);
 
        if (result) {
          this.router.navigate(['/topics/', this.topic.id]);
        }
      } catch (errorResponse: any) {
        this.error = errorResponse.error.message;
      }
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
        this.getTopic(params['topicId']);
      });

    this.postCreationForm.valueChanges.subscribe((value: any) => {
        this.error = undefined;
    });
  }
}
