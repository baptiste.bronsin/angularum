import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router, RouterOutlet } from "@angular/router";
import { PostService } from "../services/post.service";
import { FormControl, FormGroup, ReactiveFormsModule } from "@angular/forms";
import { FlatBoxComponent } from "../../../shared/components/flat-box/flat-box.component";
import { TextInputComponent } from "../../../shared/components/inputs/text-input/text-input.component";
import { MainButtonComponent } from "../../../shared/components/buttons/main-button/main-button.component";
import { LucideAngularModule } from "lucide-angular";
import { DatePipe } from "@angular/common";
import { PaginatorModule } from "primeng/paginator";
import { Post } from "../../../shared/models/post.model";
import { Pagination } from "../../../shared/models/pagination.model";
import { lastValueFrom } from "rxjs";
import { Topic } from "../../../shared/models/topic.model";
import { TopicService } from "../../topics/services/topic.service";
import { TableModule } from "primeng/table";
import { AuthenticateService } from "../../authentication/services/authenticate.service";

@Component({
    selector: 'app-post-page',
    standalone: true,
    imports: [RouterOutlet, ReactiveFormsModule, FlatBoxComponent, TextInputComponent, MainButtonComponent, LucideAngularModule, TableModule, DatePipe, PaginatorModule],
    providers: [PostService, TopicService],
    templateUrl: '../pages/post.component.html'
  })
  export class PostsPageComponent implements OnInit {
    constructor(
      private topicService: TopicService,
      private postService: PostService,
      private router: Router,
      private route: ActivatedRoute,
      private authenticateService: AuthenticateService
    ) {}

    topic: Topic | undefined | null = undefined;
    posts: Post[] | undefined = undefined;
    pagination: Pagination | undefined = undefined;

    currentPage: number = 1;

    currentUserId: string | undefined = undefined;

    postFilterForm = new FormGroup({
      content: new FormControl('')
    })

    ngOnInit() {
        this.route.params.subscribe(params => {
          this.getTopic(params['topicId']);
        });

        this.currentUserId = this.authenticateService.getDecodedAccessToken().sub;
    }

    async getTopic(topicId: string) {
      try {
        const topicResult = await this.topicService.getTopicById(topicId);

        if (topicResult === undefined) {
          this.topic = null;
          console.error('No topic found');
          return;
        }

        this.topic = await lastValueFrom(topicResult);

        await this.getPosts(topicId, null);
      } catch (error) {
        this.topic = null;
      }
    }

    async getPosts(topicId: string, content: string | null) {
      const result = await this.postService.getPosts(this.topic!.id, content, this.currentPage);
      if (result === undefined) {
        console.error('No topics found');
        return;
      }

      const resultFormatted: { pagination: Pagination, posts: Post[] } = await lastValueFrom(result);
      this.posts = resultFormatted.posts;
      this.pagination = resultFormatted.pagination;
    }

    goToTopics() {
      this.router.navigate(['topics']);
    }

    onPaginationChange(event: any): void {
      if ('page' in event) {
        this.currentPage = event.page + 1;
        this.getTopic(this.topic!.id);
      }
    }

    onUpdate(postId: string) {
      this.router.navigate([`topics/${this.topic!.id}/update-post/${postId}`]);
    }

    async onDelete(postId: string) {
      try {
        const resultDelete = await this.postService.deletePost(postId);
        if (!resultDelete) return
        await lastValueFrom(resultDelete)

        await this.getPosts(this.topic!.id, null);
      } catch (error) {
        console.error("An error occured while deleting", error);
      }
    }

    async addPost() {
      this.router.navigate([`topics/${this.topic!.id}/add-post`]);
    }

    async onFilter() {
      if (this.postFilterForm.value.content === undefined) {return;}

      this.getPosts(this.topic!.id, this.postFilterForm.value.content !== '' ? this.postFilterForm.value.content : null)
    }
}