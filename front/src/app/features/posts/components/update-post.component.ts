import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router, RouterOutlet } from '@angular/router';
import { LucideAngularModule } from 'lucide-angular';
import { FlatBoxComponent } from '../../../shared/components/flat-box/flat-box.component';
import { TextInputComponent } from '../../../shared/components/inputs/text-input/text-input.component';
import { MainButtonComponent } from '../../../shared/components/buttons/main-button/main-button.component';
import { lastValueFrom } from 'rxjs';
import { PostService } from '../services/post.service';
import { Post } from '../../../shared/models/post.model';

@Component({
  selector: 'app-update-post',
  standalone: true,
  imports: [RouterOutlet, ReactiveFormsModule, FlatBoxComponent, TextInputComponent, MainButtonComponent, LucideAngularModule, FlatBoxComponent],
  providers: [],
  templateUrl: '../pages/update-post.component.html'
})
export class UpdatePostComponent implements OnInit {
  constructor(
    private postService: PostService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  post: Post | undefined | null = undefined;

  postUpdateForm = new FormGroup({
    content: new FormControl('')
  })
  error: string | undefined = undefined;

  async getPost(postId: string) {
    try {
      const postResult = await this.postService.getPost(postId);

      if (postResult === undefined) {
        this.post = null;
        console.error('No post found');
        return;
      }

      this.post = await lastValueFrom(postResult);
      this.postUpdateForm.patchValue({ content: this.post.content });
    } catch (error) {
      this.post = null;
    }
  }

  async onSubmit() {
    if (!this.post) {return}
    if (this.postUpdateForm.invalid || !this.postUpdateForm.value.content) {
        this.error = 'Veuillez saisir tous les champs de saisies';
        return
      }
  
      try {
        const resultObs = await this.postService.updatePost(this.post.id, this.postUpdateForm.value.content);
        if (!resultObs) {
            this.error = 'Une erreur est survenue lors de la mise à jour du post';
            return;
        }

        const result = await lastValueFrom(resultObs);
 
        if (result) {
          this.router.navigate(['/topics/', this.post.topicId]);
        }
      } catch (errorResponse: any) {
        this.error = errorResponse.error.message;
      }
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
        this.getPost(params['postId']);
      });

    this.postUpdateForm.valueChanges.subscribe((value: any) => {
        this.error = undefined;
    });
  }
}
