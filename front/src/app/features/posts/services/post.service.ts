import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthenticateService } from "../../authentication/services/authenticate.service";
import { Post } from "../../../shared/models/post.model";
import { Pagination } from "../../../shared/models/pagination.model";
import { Observable, map } from "rxjs";
import { environment } from "../../../../environments/environment";
import { User } from "../../../shared/models/user.model";
import { Topic, TopicLight } from "../../../shared/models/topic.model";

@Injectable({
    providedIn: 'root'
})
export class PostService {
  constructor(
    private http: HttpClient,
    private authenticateService: AuthenticateService
  ) {}
  
  public async getPosts(topicId: string, content: string | null, page: number): Promise<Observable<{ pagination: Pagination, posts: Post[] }> | undefined> {
    const tokens = this.authenticateService.getTokens();

    if (tokens === null) {
        console.error('No tokens found');
        return undefined;
    }

    const params: any = { page: page };
    if (content !== null) {
        params.content = content;
    }

    return this.http.get(environment.apiURL + '/posts/topic/' + topicId, { headers: { Authorization: `Bearer ${tokens.accessToken}` }, params: params })
      .pipe(
        map((response: any) => {
          return {
            pagination: new Pagination(response.data.pagination.page, response.data.pagination.limit, response.data.pagination.totalAmount),
            posts: response.data.posts.map((post: any) => new Post(post.id, post.content, post.authorId, post.topicId, post.createdAt, post.updatedAt, post.hidden, new User(post.author.id, post.author.firstname, post.author.lastname, post.author.email), new TopicLight(post.topic.id, post.topic.title)))
          }
        })
      )
  }

  public async getPost(postId: string): Promise<Observable<Post> | undefined> {
    const tokens = this.authenticateService.getTokens();

    if (tokens === null) {
        console.error('No tokens found');
        return undefined;
    }

    return this.http.get(environment.apiURL + '/posts/' + postId, { headers: { Authorization: `Bearer ${tokens.accessToken}` }})
      .pipe(
        map((response: any) => {
          return new Post(response.data.id, response.data.content, response.data.authorId, response.data.topicId, response.data.createdAt, response.data.updatedAt, response.data.hidden, new User(response.data.author.id, response.data.author.firstname, response.data.author.lastname, response.data.author.email), new TopicLight(response.data.topic.id, response.data.topic.title))
        })
      )
  }

  public async addPost(topicId: string, content: string) {
    const tokens = this.authenticateService.getTokens();

    if (tokens === null) {
        console.error('No tokens found');
        return undefined;
    }

    return this.http.post(environment.apiURL + '/posts', { content: content, topicId: topicId }, { headers: { Authorization: `Bearer ${tokens.accessToken}` }})
  }

  public async updatePost(postId: string, content: string) {
    const tokens = this.authenticateService.getTokens();

    if (tokens === null) {
        console.error('No tokens found');
        return undefined;
    }

    return this.http.patch(environment.apiURL + '/posts', { id: postId, content: content }, { headers: { Authorization: `Bearer ${tokens.accessToken}` }})
  }

  public async deletePost(postId: string) {
    const tokens = this.authenticateService.getTokens();

    if (tokens === null) {
        console.error('No tokens found');
        return undefined;
    }

    return this.http.delete(environment.apiURL + '/posts/' + postId, { headers: { Authorization: `Bearer ${tokens.accessToken}` }})
  }
}