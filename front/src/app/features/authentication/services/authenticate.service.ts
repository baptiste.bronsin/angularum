import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../../environments/environment";
import { BehaviorSubject, Observable, catchError, tap, throwError } from 'rxjs';
import { Tokens } from "../../../shared/models/tokens.model";
import { Router } from "@angular/router";
import * as jwt_decode from "jwt-decode";

@Injectable({
    providedIn: 'any'
})
export class AuthenticateService {
  private tokenSubject: BehaviorSubject<Tokens | null>;
  public tokens: Observable<Tokens | null>;

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.tokenSubject = new BehaviorSubject<Tokens | null>(null);
    this.tokens = this.tokenSubject.asObservable();

    if (localStorage.getItem('tokens') == 'undefined') {
      localStorage.clear();
    } else {
      this.setTokens(JSON.parse(localStorage.getItem('tokens')!));
    }
  }

  login(email: string, password: string): Observable<any> {
    return this.http.post(environment.apiURL + '/users/signin', { email, password })
    .pipe(
      tap((response: any) => {
        this.setTokens(new Tokens(response.data.accessToken, response.data.refreshToken));
        localStorage.setItem('tokens', JSON.stringify(this.tokenSubject.value));
        return response;
      }),
      catchError(this.handleError)
    );
  }

  private setTokens(token: Tokens): void {
    this.tokenSubject.next(token);
  }

  getTokens(): Tokens | null {
    return this.tokenSubject.value;
  }

  public getDecodedAccessToken(): any {
    try {
      const tokens = this.getTokens();
      if (!tokens) return
      return jwt_decode.jwtDecode(tokens.accessToken);
    } catch(Error) {
      return null;
    }
  }

  logout(): void {
    localStorage.clear();
    this.tokenSubject.next(null);
    this.router.navigate(['/login']);
  }

  private handleError(error: any): Observable<never> {
    console.error('An error occurred:', error);
    return throwError(error);
  }
}