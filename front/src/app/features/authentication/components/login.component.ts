import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Router, RouterOutlet } from '@angular/router';
import { LucideAngularModule } from 'lucide-angular';
import { FlatBoxComponent } from '../../../shared/components/flat-box/flat-box.component';
import { TextInputComponent } from '../../../shared/components/inputs/text-input/text-input.component';
import { MainButtonComponent } from '../../../shared/components/buttons/main-button/main-button.component';
import { MessageService } from "primeng/api";
import { lastValueFrom } from 'rxjs';
import { AuthenticateService } from '../services/authenticate.service';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [RouterOutlet, ReactiveFormsModule, FlatBoxComponent, TextInputComponent, MainButtonComponent, LucideAngularModule],
  providers: [MessageService],
  templateUrl: '../pages/login.component.html'
})
export class LoginComponent implements OnInit {
  constructor(
    private authenticateService: AuthenticateService,
    private router: Router
  ) {}

  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  })
  error: string | undefined = undefined;
  
  async onSubmit() {
    if (this.loginForm.invalid || !this.loginForm.value.email || !this.loginForm.value.password) {
      alert('Please fill in all fields');
      return
    }

    try {
      const result = await lastValueFrom(this.authenticateService.login(this.loginForm.value.email, this.loginForm.value.password));
      console.log('login result', result);
      if (result) {
        this.router.navigate(['/topics']);
      }
    } catch (errorResponse: any) {
      this.error = errorResponse.error.message;
    }
  }

  ngOnInit(): void {
    if (this.authenticateService.getTokens()) {
      this.router.navigate(['/topics']);
    }
    this.loginForm.valueChanges.subscribe((value: any) => {
      this.error = undefined;
    });
  }
}
