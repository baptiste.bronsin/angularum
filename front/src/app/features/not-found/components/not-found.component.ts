import { Component } from '@angular/core';
import { MainButtonComponent } from '../../../shared/components/buttons/main-button/main-button.component';
import { Router } from '@angular/router';
import { FlatBoxComponent } from '../../../shared/components/flat-box/flat-box.component';

@Component({
  selector: 'app-not-found',
  standalone: true,
  templateUrl: '../pages/not-found.component.html',
  imports: [MainButtonComponent, FlatBoxComponent]
})
export class NotFoundComponent {
  constructor(private router: Router) {}

  handleButtonClick() {
    this.router.navigate(['/login']);
  }
}
