import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { TopicService } from './services/topic.service';

@NgModule({
  declarations: [
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [TopicService],
  bootstrap: []
})
export class TopicsModule { }
