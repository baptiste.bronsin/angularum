import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../../environments/environment";
import { Observable, map, tap } from "rxjs";
import { AuthenticateService } from "../../authentication/services/authenticate.service";
import { Tokens } from "../../../shared/models/tokens.model";
import { Pagination } from "../../../shared/models/pagination.model";
import { Topic } from "../../../shared/models/topic.model";
import { User } from "../../../shared/models/user.model";

@Injectable({
    providedIn: 'root'
})
export class TopicService {
  constructor(
    private http: HttpClient,
    private authenticateService: AuthenticateService
  ) {}

  public async getTopics(page: number, authorId: string | null, title: string | null, isOpen: boolean | null): Promise<Observable<{ pagination: Pagination, topics: Topic[] }> | undefined> {
    const params: any = { page: page };
    if (authorId !== null) {
      params.author = authorId;
    }
    if (title !== null) {
      params.title = title;
    }
    if (isOpen !== null) {
      params.isOpen = isOpen;
    }

    const tokens: Tokens | null = this.authenticateService.getTokens();

    if (tokens === null) {
        console.error('No tokens found');
        return undefined;
    }

    return this.http.get(environment.apiURL + '/topics', { headers: { Authorization: `Bearer ${tokens.accessToken}` }, params: params })
        .pipe(
            map((response: any) => {
              return {
                    pagination: new Pagination(response.data.pagination.page, response.data.pagination.limit, response.data.pagination.totalAmount),
                    topics: response.data.topics.map((topic: any) => new Topic(topic.id, topic.title, topic.description, topic.authorId, topic.createdAt, topic.updatedAt, topic.closedAt, new User(topic.author.id, topic.author.firstname, topic.author.lastname, topic.author.email)))
                }
            })
        );
  }

  public async getTopicById(topicId: string): Promise<Observable<Topic> | undefined> {
    const tokens: Tokens | null = this.authenticateService.getTokens();

    if (tokens === null) {
        console.error('No tokens found');
        return undefined;
    }

    return this.http.get(environment.apiURL + '/topics/' + topicId, { headers: { Authorization: `Bearer ${tokens.accessToken}` }})
        .pipe(
            map((response: any) => {
              return new Topic(response.data.id, response.data.title, response.data.description, response.data.authorId, response.data.createdAt, response.data.updatedAt, response.data.closedAt, new User(response.data.author.id, response.data.author.firstname, response.data.author.lastname, response.data.author.email));
            })
        );
  }

  public async createTopic(title: string, description: string): Promise<Observable<Topic> | undefined> {
    const tokens: Tokens | null = this.authenticateService.getTokens();

    if (tokens === null) {
        console.error('No tokens found');
        return undefined;
    }

    return this.http.post(environment.apiURL + '/topics', { title: title, description: description }, { headers: { Authorization: `Bearer ${tokens.accessToken}` }})
        .pipe(
            map((response: any) => {
              return new Topic(response.data.id, response.data.title, response.data.description, response.data.authorId, response.data.createdAt, response.data.updatedAt, response.data.closedAt, new User(response.data.author.id, response.data.author.firstname, response.data.author.lastname, response.data.author.email));
            })
        );
  }

  public async updateTopic(topicId: string, title: string, description: string): Promise<Observable<Topic> | undefined> {
    const tokens: Tokens | null = this.authenticateService.getTokens();

    if (tokens === null) {
        console.error('No tokens found');
        return undefined;
    }

    return this.http.patch(environment.apiURL + '/topics', { id: topicId, title: title, description: description }, { headers: { Authorization: `Bearer ${tokens.accessToken}` }})
        .pipe(
            map((response: any) => {
              return new Topic(response.data.id, response.data.title, response.data.description, response.data.authorId, response.data.createdAt, response.data.updatedAt, response.data.closedAt, new User(response.data.author.id, response.data.author.firstname, response.data.author.lastname, response.data.author.email));
            })
        );
  }

  public async deleteTopic(topicId: string): Promise<Observable<any> | undefined> {
    const tokens: Tokens | null = this.authenticateService.getTokens();

    if (tokens === null) {
        console.error('No tokens found');
        return undefined;
    }

    return this.http.delete(environment.apiURL + '/topics/' + topicId, { headers: { Authorization: `Bearer ${tokens.accessToken}` }})
  }
}