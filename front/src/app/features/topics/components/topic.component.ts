import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Router, RouterOutlet } from '@angular/router';
import { LucideAngularModule } from 'lucide-angular';
import { FlatBoxComponent } from '../../../shared/components/flat-box/flat-box.component';
import { TextInputComponent } from '../../../shared/components/inputs/text-input/text-input.component';
import { MainButtonComponent } from '../../../shared/components/buttons/main-button/main-button.component';
import { lastValueFrom } from 'rxjs';
import { TopicService } from '../services/topic.service';
import { Topic } from '../../../shared/models/topic.model';
import { Pagination } from '../../../shared/models/pagination.model';
import { TableModule } from 'primeng/table';
import { DatePipe } from '@angular/common';
import { PaginatorModule } from 'primeng/paginator';
import { AuthenticateService } from '../../authentication/services/authenticate.service';
import { ClickStopPropagation } from '../../../shared/directives/click-propagation.directive';

@Component({
  selector: 'app-topics-page',
  standalone: true,
  imports: [RouterOutlet, ReactiveFormsModule, FlatBoxComponent, TextInputComponent, MainButtonComponent, LucideAngularModule, TableModule, DatePipe, PaginatorModule, ClickStopPropagation],
  providers: [TopicService],
  templateUrl: '../pages/topic.component.html'
})
export class TopicComponent implements OnInit {
  constructor(
    private topicService: TopicService,
    private router: Router,
    private authenticateService: AuthenticateService
  ) {}

  currentPage: number = 1;
  topics: Topic[] | undefined = undefined;
  pagination: Pagination | undefined = undefined;

  topicFilterForm = new FormGroup({
    title: new FormControl('')
  })

  currentUserId: string | undefined = undefined;

  ngOnInit() {
    this.getAllTopics();
    this.currentUserId = this.authenticateService.getDecodedAccessToken().sub;
  }

  async getAllTopics(): Promise<void> {
    const result = await this.topicService.getTopics(this.currentPage, null, null, null);

    if (result === undefined) {
      console.error('No topics found');
      return;
    }

    const resultFormatted: { pagination: Pagination, topics: Topic[] } = await lastValueFrom(result);
    
    this.topics = resultFormatted.topics;
    this.pagination = resultFormatted.pagination;
  }

  async onFilter(): Promise<void> {
    if (this.topicFilterForm.value.title === undefined) {return;}

    const result = await this.topicService.getTopics(this.currentPage, null, this.topicFilterForm.value.title !== '' ? this.topicFilterForm.value.title : null, null);
    if (result === undefined) {
      console.error('No topics found');
      return;
    }

    const resultFormatted: { pagination: Pagination, topics: Topic[] } = await lastValueFrom(result);
    this.topics = resultFormatted.topics;
    this.pagination = resultFormatted.pagination;
  }

  onPaginationChange(event: any): void {
    if ('page' in event) {
      this.currentPage = event.page + 1;
      this.onFilter();
    }
  }

  goToTopic(topicId: string): void {
    this.router.navigate(['/topics', topicId]);
  }

  onUpdtate(topicId: string): void {
    this.router.navigate(['/update-topic', topicId]);
  }

  async onDelete(topicId: string) {
    try {
      const resultDelete = await this.topicService.deleteTopic(topicId);
      if (!resultDelete) return
      await lastValueFrom(resultDelete)

      await this.getAllTopics();
    } catch (error) {
      console.error("An error occured while deleting", error);
    }
  }

  addTopic(): void {
    this.router.navigate(['/add-topic']);
  }
}
