import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router, RouterOutlet } from '@angular/router';
import { LucideAngularModule } from 'lucide-angular';
import { FlatBoxComponent } from '../../../shared/components/flat-box/flat-box.component';
import { TextInputComponent } from '../../../shared/components/inputs/text-input/text-input.component';
import { MainButtonComponent } from '../../../shared/components/buttons/main-button/main-button.component';
import { TopicService } from '../services/topic.service';
import { lastValueFrom } from 'rxjs';
import { Topic } from '../../../shared/models/topic.model';

@Component({
  selector: 'app-update-topic',
  standalone: true,
  imports: [RouterOutlet, ReactiveFormsModule, FlatBoxComponent, TextInputComponent, MainButtonComponent, LucideAngularModule, FlatBoxComponent],
  providers: [TopicService],
  templateUrl: '../pages/update-topic.component.html'
})
export class UpdateTopicComponent implements OnInit {
  constructor(
    private topicService: TopicService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  topic: Topic | null | undefined = undefined;

  topicUpdateForm = new FormGroup({
    title: new FormControl(''),
    description: new FormControl('')
  })
  error: string | undefined = undefined;

  async onSubmit() {
    if (!this.topic) {return}
    if (this.topicUpdateForm.invalid || !this.topicUpdateForm.value.title || !this.topicUpdateForm.value.description) {
        this.error = 'Veuillez saisir tous les champs de saisies';
        return
      }
  
      try {
        const resultObs = await this.topicService.updateTopic(this.topic.id, this.topicUpdateForm.value.title, this.topicUpdateForm.value.description);
        if (!resultObs) {
            this.error = 'Une erreur est survenue lors de la mise à jour du topic';
            return;
        }

        const result = await lastValueFrom(resultObs);
 
        if (result) {
          this.router.navigate(['/topics']);
        }
      } catch (errorResponse: any) {
        this.error = errorResponse.error.message;
      }
  }

  async getTopic(topicId: string) {
    try {
      const topicResult = await this.topicService.getTopicById(topicId);

      if (topicResult === undefined) {
        this.topic = null;
        console.error('No topic found');
        return;
      }

      this.topic = await lastValueFrom(topicResult);
      this.topicUpdateForm.patchValue({ title: this.topic.title, description: this.topic.description });
    } catch (error) {
      this.topic = null;
    }
  
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
        this.getTopic(params['topicId']);
      });

    this.topicUpdateForm.valueChanges.subscribe((value: any) => {
        this.error = undefined;
    });
  }
}
