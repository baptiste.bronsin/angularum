import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Router, RouterOutlet } from '@angular/router';
import { LucideAngularModule } from 'lucide-angular';
import { FlatBoxComponent } from '../../../shared/components/flat-box/flat-box.component';
import { TextInputComponent } from '../../../shared/components/inputs/text-input/text-input.component';
import { MainButtonComponent } from '../../../shared/components/buttons/main-button/main-button.component';
import { TopicService } from '../services/topic.service';
import { lastValueFrom } from 'rxjs';

@Component({
  selector: 'app-add-topic',
  standalone: true,
  imports: [RouterOutlet, ReactiveFormsModule, FlatBoxComponent, TextInputComponent, MainButtonComponent, LucideAngularModule, FlatBoxComponent],
  providers: [TopicService],
  templateUrl: '../pages/add-topic.component.html'
})
export class AddTopicComponent implements OnInit {
  constructor(
    private topicService: TopicService,
    private router: Router
  ) {}

  topicCreationForm = new FormGroup({
    title: new FormControl(''),
    description: new FormControl('')
  })
  error: string | undefined = undefined;

  async onSubmit() {
    if (this.topicCreationForm.invalid || !this.topicCreationForm.value.title || !this.topicCreationForm.value.description) {
        this.error = 'Veuillez saisir tous les champs de saisies';
        return
      }
  
      try {
        const resultObs = await this.topicService.createTopic(this.topicCreationForm.value.title, this.topicCreationForm.value.description);
        if (!resultObs) {
            this.error = 'Une erreur est survenue lors de la création du topic';
            return;
        }

        const result = await lastValueFrom(resultObs);
 
        if (result) {
          this.router.navigate(['/topics']);
        }
      } catch (errorResponse: any) {
        this.error = errorResponse.error.message;
      }
  }

  ngOnInit(): void {
    this.topicCreationForm.valueChanges.subscribe((value: any) => {
        this.error = undefined;
    });
  }
}
