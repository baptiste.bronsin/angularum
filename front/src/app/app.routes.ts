import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './features/authentication/components/login.component';
import { NotFoundComponent } from './features/not-found/components/not-found.component';
import { AuthGuard } from './core/guard/auth.guard';
import { TopicComponent } from './features/topics/components/topic.component';
import { PostsPageComponent } from './features/posts/components/post.component';
import { AddTopicComponent } from './features/topics/components/add-topic.component';
import { AddPostComponent } from './features/posts/components/add-post.component';
import { UpdatePostComponent } from './features/posts/components/update-post.component';
import { UpdateTopicComponent } from './features/topics/components/update-topic.component';

export const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'topics', canActivate: [AuthGuard], component: TopicComponent },
  { path: 'add-topic', canActivate: [AuthGuard], component: AddTopicComponent },
  { path: 'update-topic/:topicId', canActivate: [AuthGuard], component: UpdateTopicComponent },
  { path: 'topics/:topicId', canActivate: [AuthGuard], component: PostsPageComponent },
  { path: 'topics/:topicId/add-post', canActivate: [AuthGuard], component: AddPostComponent },
  { path: 'topics/:topicId/update-post/:postId', canActivate: [AuthGuard], component: UpdatePostComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: '**', pathMatch: 'full', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
