## Angularum

Ce dépôt GitLab contient toute la structure de l'application `Angularum`. Celle-ci est développée avec les frameworks NestJS et Angular en TypeScript.

### Installation

1. Téléchargez le contenu de l'application depuis son dépôt distant GitLab avec Git.
    ```bash
    git clone https://gitlab.com/baptiste.bronsin/angularum.git
    ```
2. Veillez à être sur la branche `main`.
3. Veuillez vérifier que `node` et `pnpm` sont bien installés sur votre machine.
    ```bash
    node --version
    ```
    ```bash
    pnpm --version
    ```

### Back-end

Rendez-vous dans le répertoire `back/` pour la mise en place du serveur NodeJS.

1. Installez toutes les dépenses NPM pour l'application.
    ```bash
    pnpm install
    ```

2. Créez un fichier `compose.env` dans le répertoire courant. 
    ```bash
    nano compose.env
    ```
    Ce fichier est nécéssaire à la configuration de la base de données PostgreSQL, vous retrouverez toutes les variables utilisées dans le fichier `compose.example.env`.

3. Faites de même pour le fichier `.env` dans le répertoire courant.
    ```bash
    nano .env
    ```
    Ce fichier est nécéssaire au fonctionnement de l'application, vous retrouverez toutes les variables utilisées dans le fichier `.env.example`.

4. Démarrez les services de PostgreSQL.
    ```bash
    docker compose up -d
    ```

5. Générez les modèles et migrations Prisma
    ```bash
    npx prisma generate
    ```

6. Lancez l'application NestJS
    ```bash
    pnpm run dev
    ```

### Front-end Angular

Rendez-vous dans le répertoire `front/` pour la mise en place de l'application Angular.

1. Installez toutes les dépenses NPM pour l'application.
    ```bash
    pnpm install
    ```

2. Lancez l'application.
    ```bash
    ng serve
    ```
    L'application utilisera les api se trouvant à l'adresse `http://localhost:3000`.

## Version en ligne

Le projet `Angularum` a été déployé sur ma lame serveur personnelle Polytech. Vous pouvez y accéder avec l'url suivante :
`https://angularum.baptistebronsin.be`.